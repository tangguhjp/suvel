<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('users.index');
});

Route::resources([
    'welcome' => 'QuestionsController',
    'superuser' => 'SurveyResultController'
]);

Route::get('/survey','QuestionsController@survey')->name('survey');

Route::get('/thanks', function(){
    return view('users.thanks');
})->name('thanks');

Route::get('/repair', 'QuestionsController@repair')->name('repair');
Route::post('/input-repair', 'QuestionsController@inputRepair')->name('input-repair');
Route::post('/answer', 'QuestionsController@answering')->name('answer');

Route::middleware('auth')->group(function(){
    Route::get('/index', 'UserController@index');
    Route::get('/table', 'UserController@table');
    Route::get('/user', 'UserController@user'); 
    Route::post('/user/create', 'UserController@store');
    Route::get('/editAdmin/{id}', 'UserController@edit');
    Route::get('/deleteAdmin/{id}', 'UserController@delete');
    Route::post('/updateAdmin/{id}', 'UserController@update');
    
    Route::post('/updatePeriod', 'SurveyResultController@updatePeriod');

    Route::get('/export', function(){ 
        return view('admin.export');
    });
    Route::post('/exportExcel', 'SurveyResultController@exportExcel');

    Route::get('/services' , 'AnswerOptionController@index');
    Route::get('/services/edit/{id}' , 'AnswerOptionController@edit');
    Route::get('/services/delete/{id}' , 'AnswerOptionController@destroy');
    Route::post('/services/update/{id}' , 'AnswerOptionController@update');
    Route::post('/services/add' , 'AnswerOptionController@store')->name('add-service');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
