@extends('users.main')
@section('content')
  <!-- Header -->
  @include('users.header')
  <!-- #header -->

  <!-- About -->
  <section class="about bg-image" id="about">
    <div class="container text-center">
      <h2>
          {{$data['question']->question}}
      </h2>

      <div class="row stats-row">
        @foreach($data['answer'] as $answer)
          @if($answer->answer != "Buruk" && $answer->answer != "Sangat Buruk")
            <div class="stats-col text-center col-md-4 col-sm-6">
                <form action="{{route('answer')}}" method="post">
                  {{ csrf_field() }}
                  <input class="hover-custom" type="image" src="{{ asset('img') }}/{{$answer->answer}}.png" 
                    alt="Submit" width="140" height="140">
                    <br>
                  <input type="hidden" name="id_question" value="{{$data['question']->id}}">
                  <input type="hidden" name="id_answer" value="{{$answer->id}}">
                </form>
                <h3>{{$answer->answer}}</h3>
            </div>
          @endif
        @endforeach
      </div>
      <style>
        body {font-family: Arial, Helvetica, sans-serif;}
        * {box-sizing: border-box;}

        /* The popup form - hidden by default */
        .form-popup {
          display: none;
          position: fixed;
          bottom: 200px;
          right: 250px;

          z-index: 9;
        }

        /* Add styles to the form container */
        .form-container {
          max-width: 1000px;
          padding: 80px 70px;
          background-color: #1f3c88;
          opacity: 0.8;
        }

        /* Set a style for the submit/login button */
        .form-container .btn {
          background-color: #4CAF50;
          color: white;
          padding: 20px 20px;
          border: none;
          cursor: pointer;
          width: 30%;
          margin-bottom:5px;
          opacity: 0.8;
        }

        /* Add a red background color to the cancel button */
        .form-container .cancel {
          background-color: red;
        }

        /* Add some hover effects to buttons */
        .form-container .btn:hover, .open-button:hover {
          opacity: 1;
        }

        .option-2{
          margin-right: 160px;
          margin-left: 160px;
        }
      </style>
      <div class="row stats-row option-2">
        @foreach($data['answer'] as $answer)
          @if($answer->answer == "Buruk" || $answer->answer == "Sangat Buruk")
        <div class="stats-col text-center col-md-6 col-sm-6">
            <input onclick="openForm(this)" class="hover-custom" type="image" name="emot" 
              data-id_question="{{$data['question']->id}}" data-id_answer="{{$answer->id}}"
              src="{{ asset('img') }}/{{$answer->answer}}.png" alt="Submit" width="140" height="140"
              readonly>
              <br>
            <h3>{{$answer->answer}}</h3>
        </div>
          @endif
        @endforeach
        <div class="form-popup" id="myForm">
          <form action="{{route('repair')}}" class="form-container">
            <h1 style="font-size: 15pt; color: white">Apakah Anda mau membantu kami melakukan penilaian selanjutnya?</h1><br><br>
            <button type="submit" class="btn" onclick>YA</button>
            <button type="button" class="btn cancel" onclick="window.location.href='{{route('thanks')}}'">TIDAK</button>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection()