<header id="header">
    <div class="container">
      <div id="logo" class="pull-left">
        <a href="{{route('welcome.index')}}">
            <img src="{{ asset('img') }}/logo.png" alt="" title="" />
        </a>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li><a href="{{route('welcome.index')}}">Lembaga Pemasyarakatan Kelas 1 Madiun</a></li>
        </ul>
      </nav>
      <nav class="pull-right d-none d-lg-inline">
        <ul class="nav-menu pull-right">
            <a href="#"><i class="fa fa-twitter"></i> | Lapas Klas I Madiun</a> 
            <a href="#"><i class="fa fa-facebook"></i> | Lapas Madiun</a>
        </ul>
      </nav>
    </div>
</header>