@extends('users.main')
@section('content')
  <section class="hero">
    <div class="container text-center">
      <div class="row">
        <div class="col-md-12">
          <a class="hero-brand" href="index.html" title="Home"><img style="width:100px; height:100px;" alt="Bell Logo" src="{{ asset('img') }}/logo.png"></a>
        </div>
      </div>

      <div class="col-md-12">
        <h1>
            Selamat Datang
          </h1>

        <p class="tagline">
          Lembaga Pemasyarakatan Kelas 1 Madiun
        </p>
        <a class="btn btn-full" href="/survey">Mulai Sekarang</a>
      </div>
    </div>

  </section>
@endsection()