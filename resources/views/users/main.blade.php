<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>LAPAS KELAS 1 MADIUN</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <meta name="csrf-token" content="{!! Session::token() !!}">

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">

  <!-- Favicon -->
  <link href="{{ asset('img') }}/favicon.ico" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('lib') }}/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('lib') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('css') }}/style.css" rel="stylesheet">
  <link href="{{ asset('css') }}/icheck-bootstrap.css" rel="stylesheet">


</head>

<body id="main">
    @yield('content')
  <footer class="site-footer">
    <div class="bottom">
      <div class="container">
        <div class="row">

          <div class="col-lg-6 col-xs-12 text-lg-left text-center">
            <p class="copyright-text">
              © LAPAS KELAS 1 MADIUN
            </p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a>


  <!-- Required JavaScript Libraries -->
  <script src="{{ asset('lib') }}/jquery/jquery.min.js"></script>
  <script src="{{ asset('lib') }}/jquery/jquery-migrate.min.js"></script>
  <script src="{{ asset('lib') }}/superfish/hoverIntent.js"></script>
  <script src="{{ asset('lib') }}/superfish/superfish.min.js"></script>
  <script src="{{ asset('lib') }}/tether/js/tether.min.js"></script>
  <script src="{{ asset('lib') }}/stellar/stellar.min.js"></script>
  <script src="{{ asset('lib') }}/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('lib') }}/counterup/counterup.min.js"></script>
  <script src="{{ asset('lib') }}/waypoints/waypoints.min.js"></script>
  <script src="{{ asset('lib') }}/easing/easing.js"></script>
  <script src="{{ asset('lib') }}/stickyjs/sticky.js"></script>
  <script src="{{ asset('lib') }}/parallax/parallax.js"></script>
  <script src="{{ asset('lib') }}/lockfixed/lockfixed.min.js"></script>

  <script src="{{ asset('js') }}/custom.js"></script>

  <script src="{{ asset('contactform') }}/contactform.js"></script>

  <script>
    function openForm(e) {
      var id_question = e.getAttribute("data-id_question");
      var id_answer = e.getAttribute("data-id_answer");
      console.log(id_answer);
      $.post("{{route('answer')}}",{
         _token: $('meta[name=csrf-token]').attr('content'),
          id_question: id_question,
          id_answer: id_answer
      }).fail(function(){
        alert("Error");
      });

      document.getElementById("myForm").style.display = "block";

      setTimeout(function(){
          window.location.href= "{{route('survey')}}";
      }, 5000);
    }
  </script>
</body>
</html>