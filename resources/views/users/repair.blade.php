@extends('users.main')
@section('content')
  <!-- Header -->
  @include('users.header')
  <!-- #header -->

  <!-- About -->
   <section class="about bg-image" style="padding-bottom: 70px!important;">
        <div class="container">
            <div class="alert alert-info">
                <center><h4><b>{{$data['question']->question}}</b></h4></center>
            </div><!--/register-req-->
            
            <div class="shopper-informations">
                 <div class="row">
                       <div class="col-sm-6">
                          <div class="shopper-info" style="padding:80px;">
                            <form action="{{route('input-repair')}}" method="post">
                             {{ csrf_field() }}
                              @foreach($data['answer'] as $answer)
                              <div class="checkbox icheck-primary">
                                <input type="checkbox" id="{{$answer->id}}" name="id_answer[]" value="{{$answer->id}}"/>
                                <label style="font-size: 20px; font-weight: 600;" for="{{$answer->id}}">{{$answer->answer}}</label>
                                <input type="hidden" name="id_question" value="{{$data['question']->id}}">
                              </div>
                              @endforeach
                              <input type="submit" class="btn-submit-repair btn btn-full" value="Submit">
                            </form>        
                          </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="order-message">
                                <img src="img/tanya.png"  alt="" />
                            </div>  
                        </div>                
               </div>
            </div>
            
        </div>
    </section> <!--/#cart_items-->

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    setTimeout(function(){
        window.location.href= "{{route('survey')}}";
    }, 10000);
  </script>

@endsection()
