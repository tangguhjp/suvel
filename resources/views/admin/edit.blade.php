@extends('admin.main')
@section('content')
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li>User</li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Edit Data User</h3>
                            <div class="table-responsive">
                            @if(session('sukses'))
                            <div class="alert alert-success" role="alert">
                                {{session('sukses')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                                <table class="table">
                                    <thead>
                                        <div class="modal-body">
                                            <form action="/updateAdmin/{{$user->id}}" method="POST">
                                            {{csrf_field()}}
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nama</label>
                                                    <input name="name" type="text" class="form-control" id="exampleInputName" aria-describedby="nameHelp" value="{{$user -> name}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Email</label>
                                                    <input name="email" type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" value="{{$user -> email}}">
                                                    </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Password</label>
                                                    <input name="password" type="password" class="form-control" id="exampleInputPassword" value="{{$user -> password}}">
                                                </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-warning">Update</button>
                                                </div>
                                            </form>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  @endsection