<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon.png') }}">
    <title>Admin - Lapas Kelas 1 Madiun</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('admin/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/bootstrap/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{ asset('admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ asset('admin/plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{ asset('admin/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- animation CSS -->
    <!-- <link href="{{ asset('admin/animate.css') }}" rel="stylesheet"> -->
    <!-- Custom CSS -->
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/custom-style.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('admin/css/colors/blue-dark.css') }}" id="theme" rel="stylesheet">
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
                <div class="top-left-part">
                    <a class="logo" href="{{route('superuser.index')}}">
                        <b><img class="logo-admin" src="{{ asset('img/logo.png') }}" alt="home" /></b>
                        <span class="hidden-xs">
                            <b>SurveyLapas</b>
                        </span>
                    </a>
                </div>

                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <a class="profile-pic disabled" href="#">
                            <i class="fas fa-user img-circle"></i> 
                            <b class="hidden-xs">{{Auth::user()->name}}</b> 
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li style="padding: 10px 0 0;">
                        <a href="{{route('superuser.index')}}" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i><span class="hide-menu">Dashboard</span></a>
                    </li>
                    <li>
                        <a href="{{ url('/export')}} " class="waves-effect"><i class="fa fa-file-excel-o fa-fw" aria-hidden="true"></i><span class="hide-menu">Export</span></a>
                    </li>
                    <li>
                        <a href="{{ url('/user')}}" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i><span class="hide-menu">User</span></a>
                    </li>
                    <li>
                        <a href="{{ url('/services')}}" class="waves-effect"><i class="fa fa-child fa-fw" aria-hidden="true"></i><span class="hide-menu">Services</span></a>
                    </li>
                </ul>
                <hr>
                <button class="btn btn-danger btn-logout"
                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                    Logout
                </button>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        @yield('content')
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="{{ asset('admin/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('admin/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/bootstrap/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{ asset('admin/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('admin/js/waves.js') }}"></script>
    <!--Counter js -->
    <script src="{{ asset('admin/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ asset('admin/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
    <!--Morris JavaScript -->
    <script src="{{ asset('admin/plugins/bower_components/raphael/raphael-min.js') }}"></script>
    <script src="{{ asset('admin/plugins/bower_components/morrisjs/morris.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('admin/js/custom.min.js') }}"></script>
    <script src="{{ asset('admin/js/custom.js') }}"></script>
    <!-- <script src="{{ asset('admin/js/dashboard1.js') }}"></script> -->
    <script src="{{ asset('admin/plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>
    <script type="text/javascript">
    // $(document).ready(function() {
    //     $.toast({
    //         heading: 'Welcome {{Auth::user()->name}}!!',
    //         text: 'Use the predefined ones, or specify a custom position object.',
    //         position: 'top-right',
    //         loaderBg: '#ff6849',
    //         icon: 'info',
    //         hideAfter: 1000,
    //         stack: 6
    //     })
    // });

        $(document).ready(function(){
            var date_input=$('input.date-month'); //our date input has the name "date"
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'M yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
                viewMode: "months", 
                minViewMode: "months"
            };
            date_input.datepicker(options);

            var date_year=$('input.date-year'); //our date input has the name "date"
            var options={
                format: 'yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
                viewMode: "years", 
                minViewMode: "years"
            };
            date_year.datepicker(options);
        })
    </script>
</body>

</html>
