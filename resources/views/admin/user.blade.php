@extends('admin.main')
@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">User</li>
                    </ol>
                </div>

                <div class="title-box">
                    <h3 class="box-title">USER</h3>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="table-responsive">
                            @if(session('sukses'))
                            <div class="alert alert-success" role="alert">
                                {{session('sukses')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah User</button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="/user/create" method="POST">
                                                                {{csrf_field()}}
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Nama</label>
                                                                    <input name="name" type="text" class="form-control" id="exampleInputName" aria-describedby="nameHelp" placeholder="Masukan nama anda">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Email</label>
                                                                    <input name="email" type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Masukan email anda">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Password</label>
                                                                    <input name="password" type="password" class="form-control" id="exampleInputPassword" placeholder="Password">
                                                                </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </th>
                                        </thead>
                                    <tbody>
                                        @foreach($lihat as $user)
                                        <tr>
                                            <td>{{$user->id}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                <a href="/editAdmin/{{ $user->id }}"><button class="btn btn-warning" ><span class="fa fa-pencil"></span></button></a> 
                                                <a href="deleteAdmin/{{ $user->id }}" onclick="return confirm('Anda yakin ingin menghapus data <?php echo $user['name']; ?> ?')">
                                                    <button class="btn btn-danger" >
                                                        <span class="fa fa-trash"></span>
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
  @endsection