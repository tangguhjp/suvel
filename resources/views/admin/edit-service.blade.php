@extends('admin.main')
@section('content')
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li>Service</li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Edit Data Layanan</h3>
                            <div class="table-responsive">
                            @if(session('sukses'))
                            <div class="alert alert-success" role="alert">
                                {{session('sukses')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                                <table class="table">
                                    <thead>
                                        <div class="modal-body">
                                            <form action="/services/update/{{$service->id}}" method="POST">
                                            {{csrf_field()}}
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Layanan</label>
                                                    <input name="service" type="text" class="form-control" id="exampleInputName" aria-describedby="nameHelp" value="{{$service->answer }}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-warning">Update</button>
                                                </div>
                                            </form>
                                        </div>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  @endsection