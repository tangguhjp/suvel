@extends('admin.main')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                    <!-- <a href="http://wrappixel.com/templates/pixeladmin/" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Upgrade to Pro</a> -->
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
            </div>
            
            <div>
                <form id="form-period" class="form-inline">
                    @csrf
                    <div class="form-group mb-2">
                        <h3 class="text-muted">Periode : </h3>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <!-- <label for="inputPassword2" class="sr-only">Password</label> -->
                        <input class="form-control date-month" id="date-period" name="date" value="{{date('M Y', $data->today)}}" type="text"/>
                    </div>
                    <!-- <button id="btn-period" type="button" class="btn btn-primary mb-2">Apply</button> -->
                </form>
            </div>
            <hr class="dashboard">

            <div class="title-box">
                <h3 class="box-title">KEPUASAN PENGUNJUNG</h3>
            </div>

            <div id="survey-result" class="row">
                @foreach($data->survey as $d)
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="E" class="linea-icon linea-basic"></i>
                                    <h5 class="text-muted vb">{{$d->answer}}</h5> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-danger">{{$d->value}}</h3> </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$d->width}}%"> <span class="sr-only">40% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <hr class="dashboard">

            <div class="title-box">
                <h3 class="box-title">YANG PERLU DIPERBAIKI</h3>
            </div>

            <div id="repair-result" class="row">
                @foreach($data->repair as $d)
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="E" class="linea-icon linea-basic"></i>
                                    <h5 class="text-muted vb">{{$d->answer}}</h5> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-danger">{{$d->value}}</h3> </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$d->width}}%"> <span class="sr-only">40% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
                  <!-- /.row -->
            <!--row -->
            <div class="row d-none">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">Survey Difference</h3>
                        <ul class="list-inline text-right">
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #dadada;"></i>Site A View</h5>
                            </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #aec9cb;"></i>Site B View</h5>
                            </li>
                        </ul>
                        <div id="morris-area-chart2" style="height: 370px;"></div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer text-center"> 2019 &copy; Lapas Kelas 1 Madiun </footer>
    </div>
@endsection()