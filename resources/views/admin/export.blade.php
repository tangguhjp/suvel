@extends('admin.main')
@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Export</li>
                    </ol>    
                </div>

                <div class="title-box">
                    <h3 class="box-title">EXPORT</h3>
                </div>
                @if(session('error'))
                    <div class="alert alert-success" role="alert">
                        {{session('error')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <?php 
                                $thisMonth = date("M Y"); 
                                $thisYear = date("Y"); 
                            ?>
                            <form id="form-export" action="/exportExcel" method="post">
                                @csrf
                                <div class="form-inline">
                                    <div class="form-group mb-2">
                                        <h4 class="text-muted">Periode &nbsp;&nbsp;&nbsp;:</h4>
                                    </div>
                                    <div class="form-group mx-sm-3 mb-2">
                                        <select class="form-control" name="periode" id="select-period">
                                            <option value="1"selected>Bulanan</option>
                                            <option value="2">Tahunan</option>
                                            <option value="3">Custom</option>
                                        </select>
                                    </div>
                                </div>
                                <h4 class="text-muted">Range &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</h4>
                                <div id="in-range" class="input-group input-daterange">
                                    <input id="start-range" type="text" name="startDate" class="form-control date-month" value="{{$thisMonth}}">
                                    <div class="input-group-addon"> to </div>
                                    <input id="end-range" type="text" name="endDate" class="form-control date-month" value="{{$thisMonth}}">
                                </div>
                                <div id="a-month" class="input-group">
                                    <input id="month" type="text" name="month" class="form-control date-month" value="{{$thisMonth}}">
                                </div>
                                <div id="a-year" class="input-group">
                                    <input id="year" type="text" name="year" class="form-control date-year" value="{{$thisYear}}">
                                </div>
                                <br>
                                <button id="btn-export" type="submit" class="btn btn-primary mb-2">Export</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
  @endsection