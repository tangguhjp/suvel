@extends('admin.main')
@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Services</li>
                    </ol>
                </div>

                <div class="title-box">
                    <h3 class="box-title">DATA PERBAIKAN</h3>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="table-responsive">
                            @if(session('sukses'))
                            <div class="alert alert-success" role="alert">
                                {{session('sukses')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Pelayanan</th>
                                            <th><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-services">Tambah Layanan</button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="add-services" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Tambah Daftar Layanan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{route('add-service')}}" method="POST">
                                                                {{csrf_field()}}
                                                                <div class="form-group">
                                                                    <label for="service-name">Layanan</label>
                                                                    <input name="service" type="text" class="form-control" id="service-name" aria-describedby="nameHelp" placeholder="Masukan layanan">
                                                                </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </th>
                                        </thead>
                                    <tbody>
                                        <?php $no = 1; ?>
                                        @foreach($services as $service)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$service->answer}}</td>
                                            <td>
                                                <a href="/services/edit/{{ $service->id }}"><button class="btn btn-warning" ><span class="fa fa-pencil"></span></button></a> 
                                                <a href="/services/delete/{{ $service->id }}" onclick="return confirm('Anda yakin ingin menghapus layanan <?php echo $service['answer']; ?> ?')">
                                                    <button class="btn btn-danger" >
                                                        <span class="fa fa-trash"></span>
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
  @endsection