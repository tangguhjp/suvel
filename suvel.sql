-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Bulan Mei 2021 pada 09.10
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suvel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `answer_option`
--

CREATE TABLE `answer_option` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `answer_option`
--

INSERT INTO `answer_option` (`id`, `answer`) VALUES
(1, 'Sangat Puas'),
(2, 'Puas'),
(3, 'Biasa'),
(4, 'Buruk'),
(5, 'Sangat Buruk'),
(6, 'Pendaftaran Pengunjung'),
(7, 'Fasilitas Pengunjung'),
(8, 'Kinerja Petugas'),
(9, 'Kebersihan Tempat Kunjungan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_10_024250_create_questions_table', 1),
(4, '2019_05_10_024324_create_answer_option_table', 1),
(5, '2019_05_10_155119_create_question_answer_table', 1),
(6, '2019_05_10_155160_create_survey_result_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `questions`
--

INSERT INTO `questions` (`id`, `question`, `number`) VALUES
(1, 'Bagaimana Pelayanan Kami?', 1),
(2, 'Manakah Pelayanan Kami yang Kurang Baik?', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `question_answer`
--

CREATE TABLE `question_answer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_question` bigint(20) UNSIGNED NOT NULL,
  `id_answer` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `question_answer`
--

INSERT INTO `question_answer` (`id`, `id_question`, `id_answer`) VALUES
(5, 1, 1),
(6, 1, 2),
(7, 1, 3),
(8, 1, 4),
(9, 1, 5),
(10, 2, 6),
(11, 2, 7),
(12, 2, 8),
(13, 2, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `survey_result`
--

CREATE TABLE `survey_result` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_answer_id` bigint(20) UNSIGNED NOT NULL,
  `period` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `survey_result`
--

INSERT INTO `survey_result` (`id`, `question_answer_id`, `period`, `value`) VALUES
(18, 5, 1622505599, 0),
(19, 6, 1622505599, 0),
(20, 7, 1622505599, 0),
(21, 8, 1622505599, 0),
(22, 9, 1622505599, 0),
(23, 10, 1622505599, 0),
(24, 11, 1622505599, 0),
(25, 12, 1622505599, 0),
(26, 13, 1622505599, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$V5ogest576BB4GhO/tSAwOP5OOLY5hUElNmkXrbkyeJswsaJ9HBEq', 'w85xf3ejvCu5TnmrxRkEUZKHvWPt6DUoS2tlALtnnoAqTllFjSrCJnlZNinE', '2021-05-09 17:00:00', '2021-05-09 17:00:00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `answer_option`
--
ALTER TABLE `answer_option`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `question_answer`
--
ALTER TABLE `question_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_answer_id_question_foreign` (`id_question`),
  ADD KEY `question_answer_id_answer_foreign` (`id_answer`);

--
-- Indeks untuk tabel `survey_result`
--
ALTER TABLE `survey_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_result_question_answer_id_foreign` (`question_answer_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `answer_option`
--
ALTER TABLE `answer_option`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `question_answer`
--
ALTER TABLE `question_answer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `survey_result`
--
ALTER TABLE `survey_result`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `question_answer`
--
ALTER TABLE `question_answer`
  ADD CONSTRAINT `question_answer_id_answer_foreign` FOREIGN KEY (`id_answer`) REFERENCES `answer_option` (`id`),
  ADD CONSTRAINT `question_answer_id_question_foreign` FOREIGN KEY (`id_question`) REFERENCES `questions` (`id`);

--
-- Ketidakleluasaan untuk tabel `survey_result`
--
ALTER TABLE `survey_result`
  ADD CONSTRAINT `survey_result_question_answer_id_foreign` FOREIGN KEY (`question_answer_id`) REFERENCES `question_answer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
