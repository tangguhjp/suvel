<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyResult extends Model
{
    protected $table = 'survey_result';
    public $timestamps = false;

    public function question_answer(){
        return $this->belongsTo('App\QuestionAnswer');
    }
}
