<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index() {
        // $data_option = Option::all();
        // return view("admin.index",['data_option' => $data_option]);   
    }


    public function table() {
        return view("admin.tabel");   
    }

    public function user() {
        $user = User::all();
        return view("admin.user", ['lihat' => $user]);
    }

    public function store(Request $request)
    {
        $User = new User;
        $User->name = $request->name;
        $User->email = $request->email;
        $User->password = $request->password;
        $User->save();
        return redirect('/user')->with('sukses', 'Data Berhasil di Masukan');
    }

    public function edit($id) 
    {
        $user = User::find($id);
        return view('/admin/edit', ['user' => $user]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user -> update($request->all());
        return redirect('/user')->with('sukses', 'Data Berhasil di Update');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/user')->with('sukses', 'Data Berhasil di Hapus');
    }
}
