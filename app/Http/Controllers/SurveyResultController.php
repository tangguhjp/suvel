<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SurveyResult;
use App\Questions;
use App\QuestionAnswer;
use App\AnswerOption;

use Carbon\Carbon;
use DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle;

class SurveyResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $time = strtotime(Carbon::now()->EndOfMonth());
        $this->checkThisMonth($time);

        $data = new \StdClass();
        $data->today = $time;
        $data->survey = $this->getSurveyResult($time, 1);
        $data->repair = $this->getSurveyResult($time, 2);
        
        return view('admin.dashboard', compact('data'));  
    }

    public function checkThisMonth($time){
        if(!SurveyResult::where('period', $time)->exists()){
            $questionAnswer = QuestionAnswer::all()->pluck('id');
            foreach($questionAnswer as $q){
                $data[] = [
                    'question_answer_id' => $q,
                    'period' => $time,
                    'value' => 0
                ];
            }
            SurveyResult::insert($data);
        }
    }

    public function repair()
    {
        $data = SurveyResult::where('question_answer_id', 1)->sum('value');
        return view('users.repair');
    }

    public function updatePeriod(Request $request){
        $date = explode(' ',$request->date);
        $month = date("m", strtotime($date[0]));
        $year = $date[1]; 
        $time = strtotime(Carbon::createFromDate($year, $month)->endOfMonth());

        $data = new \StdClass();
        $data->today = $time;
        $data->survey = $this->getSurveyResult($time, 1);
        $data->repair = $this->getSurveyResult($time, 2);
        
        return response()->json($data);
    }

    public function getSurveyResult($time, $number){
        $data = Questions::join('question_answer', 'question_answer.id_question','questions.id')
            ->join('survey_result AS rslt', 'rslt.question_answer_id', 'question_answer.id')
            ->join('answer_option AS ans', 'ans.id','question_answer.id_answer')
            ->select('questions.*','ans.answer', 'rslt.period', 'rslt.value' )
            ->where('number', $number)
            ->where('period', $time)
            ->get();
        foreach($data as $d){
            $d['width'] = ($d->value / ($data->sum('value') ? : 1))*100;
        }
        return $data;
    }

    public function exportExcel(Request $request){
        if($request->periode == 1){
            $startDate = strtotime($request->month);
            $endDate = strtotime(Carbon::createFromTimestamp(strtotime($request->month))->EndOfMonth());
            $periode = "Periode ".$request->month."";
        }else if($request->periode == 2){
            $startDate = strtotime("Jan ".$request->year);
            $endDate = strtotime(Carbon::createFromTimestamp(strtotime("Dec ".$request->year))->EndOfMonth());
            $periode = "Periode ".$request->year."";      
        }else{
            if(strtotime($request->startDate) > strtotime($request->endDate)){
                return redirect('/export')->with('error', 'Harap mengisi range waktu dengan benar!');
            }
            $startDate = strtotime($request->startDate);
            $endDate = strtotime(Carbon::createFromTimestamp(strtotime($request->endDate))->EndOfMonth());
            $periode = "Periode ".$request->startDate."-".$request->endDate."";
        }

        $data = $this->getDataExcel($startDate, $endDate);
        return $this->generateExcel($data, $periode);
        
    }

    public function getDataExcel($startRange, $endRange){
        // $dataValue = Questions::join('question_answer', 'question_answer.id_question','questions.id')
        //         ->join('survey_result AS rslt', 'rslt.question_answer_id', 'question_answer.id')
        //         ->join('answer_option AS ans', 'ans.id','question_answer.id_answer')
        //         ->select('questions.question', 'ans.answer', DB::raw('SUM(value) as value'))
        //         ->whereBetween('period', [$startRange, $endRange])
        //         ->groupBy('rslt.question_answer_id')->get();

        $dataValue = Questions::join('question_answer', 'question_answer.id_question','questions.id')
                    ->join('survey_result AS rslt', 'rslt.question_answer_id', 'question_answer.id')
                    ->join('answer_option AS ans', 'ans.id','question_answer.id_answer')
                    ->whereBetween('period', [$startRange, $endRange])->get();

        $result = AnswerOption::all();
        foreach($result as $res){
            $res->value = 0;
            foreach($dataValue as $d){
                if($d->answer == $res->answer){
                    $res->value = (int)$d->value;
                    $res->number = $d->number;
                }
            }
        }
        return $result;
    }

    public function generateExcel($data, $periode){
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(storage_path('export-template.xlsx'));
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("B4", $periode);
        $index = 7;
        $total_pengunjung = 0;

        foreach($data as $d){
            $sheet->setCellValue("C".$index, $d->answer);
            $sheet->setCellValue("D".$index, $d->value);
            $index++;
            $total_pengunjung += isset($d->number) ? ($d->number == 1 ? $d->value : 0) : 0 ;
        }

        $sheet->setCellValue("D17", $total_pengunjung);

        return $this->downloadResult($spreadsheet);
    }

    public function downloadResult($spreadsheet){
        $file_name = Carbon::now();
        $writer = new Xlsx($spreadsheet);

        $response =  new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );

        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$file_name.'.xlsx"');
        $response->headers->set('Cache-Control','max-age=0');
        return $response;
    }
}
