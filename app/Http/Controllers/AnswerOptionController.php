<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AnswerOption;
use App\QuestionAnswer;

class AnswerOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = AnswerOption::select('answer_option.*')
            ->join('question_answer', 'question_answer.id_answer', 'answer_option.id')
            ->where('question_answer.id_question', 2)->get();
        return view("admin.list-to-fix", compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new AnswerOption();
        $data->answer = $request->service;
        if($data->save()){
            $questions_answer = new QuestionAnswer();
            $questions_answer->id_question = 2; 
            $questions_answer->id_answer = $data->id; 
            if($questions_answer->save()){
                return redirect('/services')->with('sukses', 'Data berhasil dimasukkan');
            }
        }
        return redirect('/services')->with('sukses', 'Data tidak berhasil dimasukkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = AnswerOption::find($id);
        return view('admin.edit-service', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = AnswerOption::find($id);
        $data->answer = $request->service;
        if($data->save()){
            return redirect('/services')->with('sukses', 'Data berhasil dirubah');
        }
        return redirect('/services')->with('sukses', 'Data gagal dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = AnswerOption::find($id);
        if($service->delete()){
            return redirect('/services')->with('sukses', 'Data berhasil dihapus');
        }
        return redirect('/services')->with('sukses', 'Data gagal dihapus');
    }
}
