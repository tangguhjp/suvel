<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\SurveyResult;
use App\Questions;
use App\AnswerOption;
use App\QuestionAnswer;


use Carbon\Carbon;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->result['success'] = false;
        $this->result['data'] = null;
    }


    public function index()
    {
        return view('users.index');
    }

    public function survey(){
        $data = $this->getQuestionAnswer(1);
        return view('users.survey', compact('data'));
    }

    public function repair(){
        $data = $this->getQuestionAnswer(2);
        return view('users.repair', compact('data'));
    }

    public function getQuestionAnswer($number){
        $data = [];

        $data['question'] = Questions::where('number',$number)->get()->first();
        $questionId = $data['question']->id;

        $data['answer'] = QuestionAnswer::join('answer_option AS answer', 'answer.id', 'question_answer.id_answer')
            ->select('answer.*')
            ->where('question_answer.id_question', $questionId)->get();
        
        return $data;
    }

    public function answering(Request $request){
        $question = $request->input('id_question');
        $answer = $request->input('id_answer');

        $questionAnswerId =  $this->getQuestionAnswerId($question, $answer);
        if($this->isSamePeriod($questionAnswerId)){
            $this->updatePeriod($questionAnswerId);
        }else{
            $this->newPeriod($questionAnswerId);
        }
        return redirect()->route('thanks');
    }

    public function getQuestionAnswerId($question, $answer){
        $data = QuestionAnswer::where('id_question', $question)
            ->where('id_answer', $answer)->get()->first();
        return $data->id;
    }
    
    public function isSamePeriod($id){
        if(SurveyResult::where('question_answer_id', $id)
            ->where('period', strtotime(Carbon::now()->EndOfMonth()))->exists()){
                return true;
        }
        return false;
    }

    public function newPeriod($id){
        $data = new SurveyResult();
        $data->question_answer_id = $id;
        $data->period = strtotime(Carbon::now()->EndOfMonth());
        $data->value = 1;
        if($data->save()){
            return true;
        }
        return false;
    }

    public function updatePeriod($id){
        SurveyResult::where('question_answer_id', $id)
            ->where('period', strtotime(Carbon::now()->EndOfMonth()))
            ->update([
                'value'=> DB::raw('value+1'),
            ]);
    }

    public function inputRepair(Request $request){
        $question = $request->input('id_question');
        $answer = $request->input('id_answer');
        if($answer){
            foreach($answer as $a){
                $questionAnswerId =  $this->getQuestionAnswerId($question, $a);
                if($this->isSamePeriod($questionAnswerId)){
                    $this->updatePeriod($questionAnswerId);
                }else{
                    $this->newPeriod($questionAnswerId);
                }
            }
        }else{
            return back();
        }
        return redirect()->route('thanks');
    }
}
