<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = 'questions';
    public $timestamps = false;

    public function answer_option(){
        return $this->hasMany('App\AnswerOption');
    }
}
