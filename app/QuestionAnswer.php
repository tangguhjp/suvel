<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    protected $table = 'question_answer';
    public $timestamps = false;

    public function answer_option(){
        return $this->belongsTo('App\AnswerOption');
    }

    public function questions(){
        return $this->belongsTo('App\Questions');
    }
}
